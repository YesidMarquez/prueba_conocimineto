<?php 
session_start();
$usuario=$_SESSION['usuario'];
$nombre=$_SESSION['nombre'];
$foto=$_SESSION['foto'];
$estadom="nuevo";

require 'conexion.php';

$sql1 = "select count(estado) as numero from mensajes where usuario_recep='$usuario' and estado='$estadom' order by fecha";
$result1 = $mysqli->query($sql1);
$fila1 = $result1->fetch_array(MYSQLI_ASSOC);

$sq= "select * from mensajes where usuario_recep='$usuario'";
$resul = $mysqli->query($sq);


$sql = "CALL `datos_vista`('$usuario')";
$result = $mysqli->query($sql);
$row = $result->fetch_array(MYSQLI_ASSOC);





?>


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title></title>
  <link rel="stylesheet" href="">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script type="text/javascript" charset="utf-8" async defer>
    function validatePass(){
      var pass1=document.getElementById("contraseña").value;
      var pass2=document.getElementById("contraseña2").value;
      if ( pass1== pass2) {
        return true;
      }else{
        return false;
      }
      
     
      
    }
    function previewImage(nb) {        
      var reader = new FileReader();         
      reader.readAsDataURL(document.getElementById('uploadImage'+nb).files[0]);         
      reader.onload = function (e) {             
          document.getElementById('uploadPreview'+nb).src = e.target.result;         
      };     
    }  
  </script>
</head>

<body>
  <div class="container">
    <div class="row">
  <div class="col-3">
    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
      <div>
        <img class="user" src="<?php echo $foto ?>" alt="">
        <a ><strong><?php echo $usuario ?></strong></a>
      </div>
      <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Home</a>
      <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Actualizar</a>
      <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Messages <span class="badge badge-light"><?php echo$fila1['numero']?></span></a>
    </div>
  </div>
  <div class="col-9">
    <div class="tab-content" id="v-pills-tabContent">
      <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
        <div class="card text-center mt-5">
          <div class="card-header">
            <?php echo $row['nombre']?>
          </div>
          <div class="card-body">
            <h5 class="card-title">Datos Basicos</h5>
            <div class="container">
              <div class="row">
                <div class="col mt-3">
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 col-form-label">Telefono:</label>
                    <div class="col-sm-8">
                      <input type="email" class="form-control" id="inputEmail3" value="<?php echo $row['telefono']?> " disabled>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 col-form-label">Email:</label>
                    <div class="col-sm-8">
                      <input type="email" class="form-control" id="inputEmail3" value="<?php echo $row['email']?>" disabled>
                    </div>
                  </div>            
                  
                </div>
                <div class="col">
                  <h6>Imagen de Perfil</h6> 
                  <img src="<?php echo $foto ?>" alt="">
                </div> 
              </div> 
            </div>    
          </div>
          <div class="card-footer text-muted">
            Fecha de Registro: <?php echo $row['fecha_creacion']?>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">

        <div class="card text-center mt-5">
          <div class="card-header">
            Actualizar de Datos
          </div>
          <div class="card-body">
            <h5 class="card-title"></h5>
            <div class="container">
              <form action="cru.php" method="POST" accept-charset="utf-8" enctype="multipart/form-data" onsubmit="return validatePass();">
                <div class="container">
                  <div class="row">      
                    <div class="col">
                      <label class="mt-2">Nombre</label>
                      <input type="text" class="form-control" value="<?php echo $row['nombre']?>" name="nombre" placeholder="Nombre" required>
                      <label class="mt-2">Telefono</label>
                      <input type="" class="form-control" value="<?php echo $row['telefono']?>" name="telefono" placeholder="Telefono">
                      <label class="mt-2">Edad</label>
                      <input type="" class="form-control" value="<?php echo $row['edad']?>" name="edad" placeholder="Edad">
                      <label class="mt-2">Email</label>
                      <input type="email" class="form-control" value="<?php echo $row['email']?>" name="email" placeholder="Email" required> 
                      <label class="mt-2">Usuario</label>
                      <input type="text" class="form-control" value="<?php echo $row['usuario']?>" name="usuario" placeholder="Usuario" required>
                    </div>
                    <div class="col">
                      <label class="mt-2">Contraseña</label>
                      <input type="password" class="form-control" name="contraseña" id="contraseña" placeholder="Contraseña" required>
                      <label class="mt-2">Confirmar Contraseña</label>
                      <input type="password" class="form-control" name="contraseña" id="contraseña2"placeholder="Contraseña" required><center>
                      <div class="card mt-2" style="width: 15rem;">
                        <img src="<?php echo $row['foto'] ?>"  id="uploadPreview2" class="card-img-top img-responsive" width="100" height="150"  />
                        <div class="card-body">
                          <div class="custom-file">                                        
                            <input class="custom-file-input" id="uploadImage2" name="foto" width="300" height="300" type="file" name="images[2]" onchange="previewImage(2);" />                  
                            <label class="custom-file-label btn-primary" for="customFileLang" id="imagen1">Cargar Imagen</label>                
                          </div>
                        </div>
                      </div></center>
                      <input name="transaccion" value="creacion" style="display: none">
                    </div>           
                  </div>
                  <center><button class="btn btn-primary mt-5" type="submit" onclick="validatePass();">Guardar</button></center>
                </div>
              </form> 
            </div>    
          </div>
          <div class="card-footer text-muted">
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
        <div class="container">
          <div class="row"> 
            <div  class="col">
              <h2 class="text-center mt-5">Bandeja de Mensajes</h2>
              <table class="table mt-3 text-center">
                <thead >
                  <tr>
                    <th>Quien Envia</th>
                    <th>Mensaje</th>
                    <th>Estado</th>
                  </tr>
                </thead>
                <tbody>
                  <?php while($fila = $resul->fetch_array(MYSQLI_ASSOC)) { ?>
                  <tr>
                    <td><?php echo $fila['usuario_emi'] ?></td>
                    <td><?php echo $fila['contenido'] ?></td>
                    <td>
                      <select class="custom-select my-1 mr-sm-2" >
                        <option value="<?php echo $fila['estado'] ?>"><?php echo $fila['estado'] ?></option>
                        <option value="visto">Visto</option>
                    </select>
                  </td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table> 
              <div class="form-group text-right" >
                <button class="btn btn-primary" type="button">Actualizar</button>
                <button class="btn btn-info" type="button">Nuevo Mensaje</button>
              </div>
                   
            </div>
          </div> 
        </div>
      </div>
      <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">Mensaje</div>
    </div>
  </div>
</div>
  
</body>
</html>